# GitAndMarkdown -- Evaluation project 


> ❗️ The report should be finalized by March 4th ! 

Welcome to the repository where you have to push the markdown report associated to the git lab.

## 💪 What you have to do 

Write a one-page essay describing a GIT repo and push the repo in a dedicated branch on this repo ! 


## 📖 The essay

You will have to write a report (one page, so 40 to 50 lines) in English dedicated to an open source git repository of your choice.
The git repository has to be public and open source for instance host on (GitHub)[https://github.com] 
Some example of well known and established GitHub repositories 
- The complete [Linux kernel](https://github.com/torvalds/linux)
- A well known machine learning [Tensor Flow](https://github.com/tensorflow/tensorflow)
- The very best [editor](https://github.com/vim/vim)
- Another [very good one](https://github.com/microsoft/vscode)
- Python in [micro-controler](https://github.com/micropython/micropython)

And so forth....


Choice a project of your choice and write an essay on how the project is build and monitored with git.
This essay has to be written in **markdown** with at least 
- One figure (but no more than 3) 
- One table 
- One list 
- One external URL link.
- 2 (or more !) headings.
  - You can for example write your essay in three parts. The first one is what is the project and why you have chosen it. A second part can be the description of the repository (number of collaborators, commits, if it has external documentation, …). The last part can be dedicated to Pull Request (PR) and issue management which are powerfully assessed with GIT.
  - Do not hesitate to explore Markdown syntax. You can find valuable resources on the internet like [here for instance](https://www.markdownguide.org/cheat-sheet/)  

If you want to have a rendering of you markdown code, you can use the gitlab preview (viewing your file in gitlab after a `push`) or markdown preview in  VSCode. You can also use third party clients such as [hackmd](https://hackmd.io).  

## 🚀 The push 

To validate your work, you have to push your essay on this repo 
- This should be done in a dedicated branch (use your name as the branch name) as you can not push into master/main branch. The branch is indeed [protected](https://docs.gitlab.com/ee/user/project/protected_branches.html) 
- Your essay should have a name that will not lead to conflict (use your name as the essay name !)
- After you have push you should ask for a pull request to merge your branch with master. I will do the PR after the deadline 


⚠️a Do not wait for the last version of your essay (and five minutes before the deadline) to propose a PR. You should do a first try before your final version to be sure it will work. After you have finished, you can delete your PR and propose a new one. On the contrary to not leave several PR open ! I should have only one branch per student.  
-> You must make at least two commits on your branch ! 


## 🎁 After the deadline 

I will merge all the PR and get all the markdown files. I will generate a PDF version of your markdown file with [pandoc](https://pandoc.org/MANUAL.html).

